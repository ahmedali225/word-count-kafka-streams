package com.udemy.kafka.streams;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.kstream.KTable;

import java.util.Arrays;
import java.util.Properties;

/**
 * Created by aali on 2019-07-27
 **/
public class WordCountApp {

    public static void main(String[] args) {
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "wordcount-app");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        KStreamBuilder streamBuilder = new KStreamBuilder();
        final KStream<String, String> wordCountInput = streamBuilder.stream("word-count-input");

        final KTable<String, Long> wordCounts = wordCountInput.mapValues(textline -> textline.toLowerCase())
                .flatMapValues(lowerCasedTextLine -> Arrays.asList(lowerCasedTextLine.split(" ")))
                .selectKey((key, word) -> word)
                .groupByKey()
                .count("Counts");

        wordCounts.to(Serdes.String(), Serdes.Long(),"word-count-output");

        KafkaStreams kafkaStreams = new KafkaStreams(streamBuilder, config);
        kafkaStreams.start();

        // Prints the topology
        System.out.println(kafkaStreams.toString());

        //Shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));
    }
}
